from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.models import *
# Create your views here.

@login_required
@never_cache
def home(request, template="ig/dashboard/home.html"):
    invoices = Invoice.objects.all()
    return render_to_response(template, {
        'invoices': invoices,
    }, context_instance=RequestContext(request))
    

def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")    

