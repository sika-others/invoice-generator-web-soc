import datetime

from app.ig.models import *

def new_number():
    try:
        return Invoice.objects.order_by("-number")[0].number+1
    except:
        return 1

def date1():
    return datetime.date.today()
    
    
def date2():
    return date1()+datetime.timedelta(days=14)

def actual_seller():
    obj = Settings.objects.get(pk=1)
    return obj.seller.pk
    
def quantity_unit():
    obj = Settings.objects.get(pk=1)
    return obj.quantity_unit
    
def vat():
    obj = Settings.objects.get(pk=1)
    return obj.vat
    
def vat_include():
    obj = Settings.objects.get(pk=1)
    return obj.vat_include
    
def cs():
    obj = Settings.objects.get(pk=1)
    return obj.cs

def ss():
    obj = Settings.objects.get(pk=1)
    return obj.ss
