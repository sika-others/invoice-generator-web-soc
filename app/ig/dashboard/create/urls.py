from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^$',
        'app.ig.dashboard.create.views.invoice',
        name = "ig.dashboard.create.invoice",
    ),
    url(
        r'^(?P<invoice_pk>\w+)/items$',
        'app.ig.dashboard.create.views.items',
        name = "ig.dashboard.create.items",
    ),
)


