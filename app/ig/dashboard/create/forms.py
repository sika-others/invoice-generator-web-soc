# -*- coding: utf-8 -*- 
import datetime

from django import forms
from django.utils.translation import ugettext

from app.ig.models import *
from app.ig.dashboard.create.helpers import *


class InvoiceForm(forms.ModelForm):
    number = forms.IntegerField(initial=new_number, label=ugettext(u"číslo faktury"))
    vs = forms.IntegerField(initial=new_number, label=("variabilní symbol"))
    cs = forms.IntegerField(initial=cs, label=("konstantní symbol"))
    ss = forms.IntegerField(initial=ss, label=("specifický symbol"))
    date_create = forms.DateField(initial=date1, label=ugettext(u"Datum vytvoření"))
    date_maturity = forms.DateField(initial=date2, label=ugettext(u"Datum splatnosti"))
    date_taxable = forms.DateField(initial=date1, label=ugettext(u"Datum zdanitelného plnění"))
    seller = forms.ModelChoiceField(Seller.objects.all(),  initial=actual_seller, label=ugettext(u"Prodejce"))
    class Meta:
        model = Invoice
        exclude = ["user"]
        
        
class InvoiceItemForm(forms.ModelForm):
    vat = forms.IntegerField(initial=vat, label=ugettext(u"DPH v %"))
    vat_include = forms.BooleanField(initial=vat_include, label=ugettext(u"Daň zahrnuta v ceně"))
    quantity_unit = forms.CharField(max_length=20, initial=quantity_unit)
    class Meta:
        model = InvoiceItem
        exclude = ["invoice", "sku"]
        
