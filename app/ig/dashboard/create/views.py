from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.models import *
from app.ig.dashboard.create.forms import *

from app.ig.dashboard.create.helpers import *
# Create your views here.

@login_required
@never_cache
def invoice(request, template="ig/dashboard/create/invoice.html"):
    if request.method == 'POST':
        form = InvoiceForm(request.POST) 
        if form.is_valid(): 
            obj = Invoice()
            obj.number = form.cleaned_data['number']
            obj.seller = form.cleaned_data['seller']
            obj.customer = form.cleaned_data['customer']
            obj.user = request.user
            obj.vs = form.cleaned_data['vs']
            obj.cs = form.cleaned_data['cs']
            obj.ss = form.cleaned_data['ss']
            obj.date_create = form.cleaned_data['date_create']
            obj.date_maturity = form.cleaned_data['date_maturity']
            obj.date_taxable = form.cleaned_data['date_taxable']
            obj.save()
            return HttpResponseRedirect(reverse('ig.dashboard.create.items', args=[obj.pk]))
            
    else:
        form = InvoiceForm() 

    return render_to_response(template, {
        'form': form,
    }, context_instance=RequestContext(request))
    

@login_required
@never_cache
def items(request, invoice_pk, template="ig/dashboard/create/items.html"):
    if request.method == 'POST': # If the form has been submitted...
        form = InvoiceItemForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            obj = InvoiceItem()
            obj.user = request.user
            obj.invoice_id = int(invoice_pk)
            obj.name = form.cleaned_data['name']
            obj.price = form.cleaned_data['price']
            obj.vat = form.cleaned_data['vat']
            obj.vat_include = form.cleaned_data['vat_include']
            obj.quantity = form.cleaned_data['quantity']
            obj.quantity_unit = form.cleaned_data['quantity_unit']
            obj.save()
            if request.POST["finish"] == "true" :
                return HttpResponseRedirect(reverse('ig.generator.generate', args=[invoice_pk]))
            return HttpResponseRedirect(reverse('ig.dashboard.create.items', args=[invoice_pk]))
            
    else:
        form = InvoiceItemForm() # An unbound form

    return render_to_response(template, {
        'form': form,
    }, context_instance=RequestContext(request))
