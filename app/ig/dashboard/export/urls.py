from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^xml/',
        include('app.ig.dashboard.export.xml.urls'),
    ),
)


