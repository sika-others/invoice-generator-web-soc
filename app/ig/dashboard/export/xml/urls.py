from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^$',
        'app.ig.dashboard.export.xml.views.home',
        name = "ig.dashboard.export.xml",
    ),
    url(
        r'^download/(?P<xml_pk>\w+)$',
        'app.ig.dashboard.export.xml.views.download',
        name = "ig.dashboard.export.xml.download",
    ),
)


