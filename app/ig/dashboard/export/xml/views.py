from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.models import *
from app.ig.dashboard.export.forms import *

from app.ig.export.xml.helpers import *
from app.ig.export.xml.models import *
# Create your views here.

@login_required
@never_cache
def home(request, template="ig/dashboard/export/xml/home.html"):
    form = ExportForm(request.POST or None) 
    if form.is_valid(): 
        xml_file = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<invoices>\n"
        for invoice in form.cleaned_data["invoices"]:
           xml_file += xml_generate(invoice.pk)
        xml_file += "</invoices>"           
        xml = ExportXml(content=xml_file)
        xml.save()
        return HttpResponseRedirect(reverse('ig.dashboard.export.xml.download', args=[xml.pk]))
            
    return render_to_response(template, {
        'form': form,
    }, context_instance=RequestContext(request))
    
def download(request, xml_pk, template="ig/dashboard/export/xml/download.html"):
    xml = ExportXml.objects.get(pk=xml_pk)
    return render_to_response(template, {
        'xml': xml,
    }, context_instance=RequestContext(request))
