from django import forms

from app.ig.models import *

class ExportForm(forms.Form):
    invoices = forms.ModelMultipleChoiceField(queryset=Invoice.objects.all())

