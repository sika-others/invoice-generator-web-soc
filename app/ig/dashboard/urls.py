from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^$',
        'app.ig.dashboard.views.home',
        name = "ig.dashboard.home",
    ),
    url(
        r'^logout$',
        'app.ig.dashboard.views.logout_view',
        name = "ig.dashboard.logout",
    ),
    url(
        r'^create/',
        include('app.ig.dashboard.create.urls'),
    ),
    url(
        r'^edit/',
        include('app.ig.dashboard.edit.urls'),
    ),
    url(
        r'^adresar/',
        include('app.ig.dashboard.adresar.urls'),
    ),
    url(
        r'^export/',
        include('app.ig.dashboard.export.urls'),
    ),
    url(
        r'^settings/',
        include('app.ig.dashboard.settings.urls'),
    ),
)


