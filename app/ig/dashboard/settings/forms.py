import datetime

from django import forms

from app.ig.models import *



class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        exclude = ["user", "profile_name"]

