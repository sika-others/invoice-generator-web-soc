from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^$',
        'app.ig.dashboard.adresar.views.home',
        name = "ig.dashboard.adresar",
    ),
    url(
        r'^seller/',
        include('app.ig.dashboard.adresar.seller.urls'),
    ),
    url(
        r'^customer/',
        include('app.ig.dashboard.adresar.customer.urls'),
    ),
)


