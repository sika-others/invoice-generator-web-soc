from app.ig.models import *
from app.ig.dashboard.adresar.forms import *

class SellerForm(AdresarForm):
    class Meta:
        model = Seller
        exclude = ["user"]
