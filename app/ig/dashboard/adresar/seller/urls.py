from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^$',
        'app.ig.dashboard.adresar.seller.views.home',
        name = "ig.dashboard.adresar.seller",
    ),
    url(
        r'^add$',
        'app.ig.dashboard.adresar.seller.views.create',
        name = "ig.dashboard.adresar.seller.create",
    ),
    url(
        r'^edit/(?P<seller_pk>\w+)$',
        'app.ig.dashboard.adresar.seller.views.edit',
        name = "ig.dashboard.adresar.seller.edit",
    ),
)


