from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.models import *
from app.ig.dashboard.adresar.customer.forms import *
# Create your views here.

@login_required
@never_cache
def home(request, template="ig/dashboard/adresar/customer/home.html"):
    adress_pl = Customer.objects.all()
    return render_to_response(template, {
        'adress_pl': adress_pl,
        "type":"c",
    }, context_instance=RequestContext(request))
    
def create(request, template="ig/dashboard/adresar/form.html"):
    if request.method == 'POST':
        form = CustomerForm(request.POST) 
        if form.is_valid(): 
            obj = form.save(commit=False)
            obj.user = request.user
            obj.save()
            return HttpResponseRedirect(reverse('ig.dashboard.adresar'))
            
    else:
        form = CustomerForm() 

    return render_to_response(template, {
        'form': form,
    }, context_instance=RequestContext(request))
    
def edit(request, customer_pk, template="ig/dashboard/adresar/form.html"):
    obj = Customer.objects.get(pk=customer_pk)
    if request.method == 'POST':
        form = CustomerForm(request.POST, instance=obj) 
        if form.is_valid(): 
            form.save()
            return HttpResponseRedirect(reverse('ig.dashboard.adresar.customer'))
            
    else:
        form = CustomerForm(instance=obj) 

    return render_to_response(template, {
        'form': form,
    }, context_instance=RequestContext(request))
