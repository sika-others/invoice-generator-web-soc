from app.ig.models import *
from app.ig.dashboard.adresar.forms import *

class CustomerForm(AdresarForm):
    class Meta:
        model = Customer
        exclude = ["user"]
