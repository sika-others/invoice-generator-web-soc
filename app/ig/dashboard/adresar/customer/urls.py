from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^$',
        'app.ig.dashboard.adresar.customer.views.home',
        name = "ig.dashboard.adresar.customer",
    ),
    url(
        r'^add$',
        'app.ig.dashboard.adresar.customer.views.create',
        name = "ig.dashboard.adresar.customer.create",
    ),
    url(
        r'^edit/(?P<customer_pk>\w+)$',
        'app.ig.dashboard.adresar.customer.views.edit',
        name = "ig.dashboard.adresar.customer.edit",
    ),
)


