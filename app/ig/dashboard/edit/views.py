from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.models import *
from app.ig.dashboard.edit.forms import *

# Create your views here.

@login_required
@never_cache
def invoice(request, invoice_pk, template="ig/dashboard/edit/invoice.html"):
    obj = Invoice.objects.get(pk=invoice_pk)
    if request.method == 'POST':
        form = InvoiceForm(request.POST) 
        if form.is_valid(): 
            obj.seller = form.cleaned_data['seller']
            obj.customer = form.cleaned_data['customer']
            obj.user = User.objects.get(pk=1)
            obj.vs = form.cleaned_data['vs']
            obj.cs = form.cleaned_data['cs']
            obj.ss = form.cleaned_data['ss']
            obj.date_create = form.cleaned_data['date_create']
            obj.date_maturity = form.cleaned_data['date_maturity']
            obj.date_taxable = form.cleaned_data['date_taxable']
            obj.save()
            return HttpResponseRedirect(reverse('ig.dashboard.edit.items', args=[obj.pk]))
            
    else:
        form = InvoiceForm(instance=obj) 

    return render_to_response(template, {
        'form': form,
    }, context_instance=RequestContext(request))
    

@login_required
@never_cache
def items(request, invoice_pk, template="ig/dashboard/edit/items.html"):
    InvoiceItemFormSet = modelformset_factory(InvoiceItem, exclude=["sku", "invoice"], extra=0)
    if request.method == 'POST': 
        formset = InvoiceItemFormSet(request.POST, request.FILES)
        if formset.is_valid():
            for form in formset:
                form.save()
                return HttpResponseRedirect(reverse('ig.dashboard.home'))
    else:
        formset = InvoiceItemFormSet(queryset=InvoiceItem.objects.filter(invoice=invoice_pk)) 

    return render_to_response(template, {
        'formset': formset,
    }, context_instance=RequestContext(request))
