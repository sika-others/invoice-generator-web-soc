from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^(?P<invoice_pk>\w+)$',
        'app.ig.dashboard.edit.views.invoice',
        name = "ig.dashboard.edit.invoice",
    ),
    url(
        r'^(?P<invoice_pk>\w+)/items$',
        'app.ig.dashboard.edit.views.items',
        name = "ig.dashboard.edit.items",
    ),
)


