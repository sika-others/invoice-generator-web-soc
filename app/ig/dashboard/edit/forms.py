import datetime

from django import forms

from app.ig.models import *



class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        exclude = ["user", "number"]

