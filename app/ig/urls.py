from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^dashboard/',
        include('app.ig.dashboard.urls'),
    ),
    url(
        r'^generator/',
        include('app.ig.generator.urls'),
    ),
        url(
        r'^export/',
        include('app.ig.export.urls'),
    ),
)


