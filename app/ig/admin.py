
from django.contrib import admin
from app.ig.models import *


admin.site.register(Customer)
admin.site.register(Seller)
admin.site.register(Invoice)
admin.site.register(InvoiceItem)
admin.site.register(InvoicePdf)
admin.site.register(Template)
admin.site.register(Settings)

