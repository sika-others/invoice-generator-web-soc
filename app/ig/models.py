# -*- coding: utf-8 -*- 

import os

from django.db import models
from django.utils.translation import ugettext

import settings

# Create your models here.

from django.contrib.auth.models import User

class Adresar(models.Model):
    name = models.CharField(max_length=80, verbose_name=ugettext(u"Jméno"))
    user = models.ForeignKey(User)
    adress = models.TextField(blank=True, verbose_name=ugettext(u"Adresa"))
    ic = models.CharField(max_length=20, blank=True, verbose_name=ugettext(u"IČ"))
    dic = models.CharField(max_length=20, blank=True, verbose_name=ugettext(u"DIČ"))
    bank_account = models.CharField(max_length=80, blank=True, verbose_name=ugettext(u"Účet"))
    other = models.TextField(blank=True, verbose_name=ugettext(u"Ostatní"))
    
    def adress_inline(self):
        return self.adress.replace("\n", ", ")
    
    def __unicode__(self):
        return u"%s ~%d"%(self.name, self.pk)
    
                
class Seller(Adresar):
    class Meta:
        verbose_name = ugettext(u"Prodejce")
        verbose_name_plural = ugettext(u"Prodejci")
    
class Customer(Adresar):
    class Meta:
        verbose_name = ugettext(u"Zákazník")
        verbose_name_plural = ugettext(u"Zákazníci")


class Invoice(models.Model):

    number = models.IntegerField(unique=True, verbose_name=ugettext(u"Číslo faktury"))
    
    seller = models.ForeignKey(Seller, verbose_name=ugettext(u"Prodejce"))
    customer = models.ForeignKey(Customer, verbose_name=ugettext(u"Zákazník"))
    user = models.ForeignKey(User)
    
    vs = models.CharField(max_length=20, blank=True, verbose_name=ugettext(u"variabilní symbol"))
    cs = models.CharField(max_length=20, blank=True, verbose_name=ugettext(u"konstantní symbol"))
    ss = models.CharField(max_length=20, blank=True, verbose_name=ugettext(u"specifiký symbol"))
    
    date_create = models.DateField(verbose_name=ugettext(u"Datum vytvoření (rrrr-mm-dd)"))
    date_maturity = models.DateField(verbose_name=ugettext(u"Datum splatnosti (rrrr-mm-dd)"))
    date_taxable = models.DateField(verbose_name=ugettext(u"Datum zdanitelného plnění (rrrr-mm-dd)"))
    
    def price(self):
        x = 0
        for item in self.invoiceitem_set.all():
            x += item.get_price()*item.quantity
        return x
        
        
    def vat(self):
        x = 0
        for item in self.invoiceitem_set.all():
            x += item.get_vat()*item.quantity
        return x
        
    def price_vat(self):
        x = 0
        for item in self.invoiceitem_set.all():
            x += item.get_price_vat()*item.quantity
        return x    
        
        
    def path(self):
        return self.invoicepdf_set.order_by("-pk")[0].path()
        
    def __unicode__(self):
        return str(self.number)+" ~"+str(self.pk)
    
    class Meta:
        verbose_name = ugettext(u"Faktura")
        verbose_name_plural = ugettext(u"Faktury")
        
class InvoiceItem(models.Model):
    invoice = models.ForeignKey(Invoice)

    sku = models.CharField(max_length=20, blank=True, verbose_name=ugettext(u"skladové číslo"))
    name = models.CharField(max_length=80, verbose_name=ugettext(u"Název položky"))
    price = models.IntegerField(verbose_name=ugettext(u"Cena"))
    vat = models.IntegerField(verbose_name=ugettext(u"Daň"))
    vat_include = models.BooleanField(verbose_name=ugettext(u"Daň zahrnuta v ceně"))
    quantity = models.IntegerField(verbose_name=ugettext(u"Množství"))
    quantity_unit = models.CharField(max_length=20, verbose_name=ugettext(u"Množstevní jednotka"))
    
    def get_price(self):
        if self.vat_include :
            return self.price-(self.price*self.vat/100) # funkcni
        else :
            return self.price
        
    def get_vat(self):
        if self.vat_include :
            return self.price*self.vat/100
        else :
            return self.price*self.vat/100
        
    def get_price_vat(self):# funkcni
        if self.vat_include :
            return self.price
        else :
            return self.price+(self.price*self.vat/100)
    
    def __unicode__(self):
        return str(self.invoice)+" ~"+str(self.pk)
        
    class Meta:
        verbose_name = ugettext(u"Položka faktury")
        verbose_name_plural = ugettext(u"Položky faktur")
        
class InvoicePdf(models.Model):
    invoice = models.ForeignKey(Invoice)
    
    def path(self):
        return os.path.normpath(os.path.join(settings.PROJECT_ROOT, "var", "invoices", u"%i.pdf"%self.pk))
        
    class Meta:
        verbose_name = ugettext(u"Faktura v PDF")
        verbose_name_plural = ugettext(u"Faktury v PDF")
    
class Template(models.Model):
    name = models.CharField(max_length=64)
    active = models.BooleanField(default=True)
    content = models.TextField()
    item = models.TextField()
    
    class Meta:
        verbose_name = ugettext(u"Šablona")
        verbose_name_plural = ugettext(u"Šablony")
        
        
class Settings(models.Model):
    profile_name = models.CharField(max_length=20, verbose_name=ugettext(u"Název profilu"))
    user = models.ForeignKey(User)
    seller = models.OneToOneField(Seller, verbose_name=ugettext(u"Prodejce"))
    cs = models.IntegerField(blank=True, verbose_name=ugettext(u"konstantní symbol"))
    ss = models.IntegerField(blank=True, verbose_name=ugettext(u"specifický symbol"))
    vat = models.IntegerField(blank=True, verbose_name=ugettext(u"Daň"))
    vat_include = models.BooleanField(verbose_name=ugettext(u"Daň zahrnuta v ceně"))
    quantity_unit = models.CharField(blank=True, max_length=20, verbose_name=ugettext(u"Množstevní jednotka"))
    
    def __unicode__(self):
        return u"%s ~%d"%(self.profile_name, self.pk)
        
    class Meta:
        verbose_name = ugettext(u"Nastavení")
        verbose_name_plural = ugettext(u"Nastavení")
