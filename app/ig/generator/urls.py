from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^generate/(?P<invoice_pk>\w+)$',
        'app.ig.generator.views.generate',
        name = "ig.generator.generate",
    ),
    url(
        r'^download/(?P<invoice_pk>\w+)$',
        'app.ig.generator.views.download',
        name = "ig.generator.download",
    ),
)


