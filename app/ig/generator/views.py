from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.models import *

import ltic
# Create your views here.

@login_required
@never_cache
def generate(request, invoice_pk, redirect=None):
    pdf_pk = ltic.generate(invoice_pk, 1)
    if not redirect:
        redirect = reverse("ig.generator.download", args=[invoice_pk])
        redirect = "/" # !!!
    return HttpResponseRedirect(redirect)
    
def download(request, invoice_pk):
    obj = Invoice(pk=invoice_pk)
    f = file(obj.path(), "r")
    content = f.read()
    f.close()
    return HttpResponse(content, content_type="file")
