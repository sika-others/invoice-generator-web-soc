# -*- coding:utf-8 -*-

from app.ig.models import *

def replacer(
        invoice_pk,
        main_template,
        item_template,
        ):
        
    invoice = Invoice.objects.get(pk=invoice_pk)
    items = invoice.invoiceitem_set.all()

    item_content = ""
    for item in items:
        item_content2 = item_template
        item_replaces = [
            ("name", item.name),
            ("quantity",item.quantity),
            ("quantity_unit",item.quantity_unit),
            ("price_quantity_unit",item.get_price_vat()),
            ("price_quantity",item.get_price_vat()*item.quantity),
        ]
        for repl in item_replaces:
            item_content2 = item_content2.replace("~%s~"%repl[0], u"%s"%repl[1])
        item_content += u"\n%s"%item_content2
    
    replaces = [
        ("~items~",item_content),
        ("number",invoice.number),
        ("price",invoice.price()),
        ("vat",invoice.vat()),
        ("price_vat",invoice.price_vat()),
        ("vs",invoice.vs),
        ("cs",invoice.cs),
        ("ss",invoice.ss),
        ("date_create",invoice.date_create),
        ("date_maturity",invoice.date_maturity),
        ("date_taxable",invoice.date_taxable),
        ("seller.name", invoice.seller.name),
        ("seller.adress", invoice.seller.adress.replace("\n","\n\n")),
        ("seller.ic", invoice.seller.ic),
        ("seller.dic", invoice.seller.dic),
        ("seller.bank_account", invoice.seller.bank_account),
        ("seller.other", invoice.seller.other.replace("\n","\n")),
        ("customer.name", invoice.customer.name),
        ("customer.adress", invoice.customer.adress.replace("\n","\n\n")),
        ("customer.ic", invoice.customer.ic),
        ("customer.dic", invoice.customer.dic),
        ("customer.bank_account", invoice.customer.bank_account),
        ("customer.other", invoice.customer.other.replace("\n","\n")),
    ]
    
    for repl in replaces:
        main_template = main_template.replace("~%s~"%repl[0], u"%s"%repl[1])

    return main_template
