# -*- coding:utf-8 -*-

import os
import sys

import settings

import replacer

from app.ig.models import *

def generate(
        invoice_pk,
        template_pk,
        ):
    
    invoice = Invoice.objects.get(pk=invoice_pk)
    template = Template.objects.get(pk=template_pk)
    
    tex_file = os.path.normpath(os.path.join(settings.PROJECT_ROOT, "tmp", "generate.tex"))
    generate_dir = os.path.normpath(os.path.join(settings.PROJECT_ROOT, "tmp"))

    f = file(tex_file, "w")
    f.write(replacer.replacer(invoice_pk, template.content, template.item).encode('utf-8'))
    f.close()
    
    pdf = InvoicePdf(invoice=invoice)
    pdf.save()
    
    os.system("cd %s ; pdflatex %s > /dev/null ; cp generate.pdf %s"%(generate_dir, tex_file, pdf.path()))
    return pdf.pk
    
