from app.ig.export.xml.models import *
from ig.generator.replacer import * 

def xml_generate(invoice_pk):
    template = TemplateXml.objects.get(pk=1)
    return replacer(invoice_pk, template.content, template.item) 
