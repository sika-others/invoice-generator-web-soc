
from django.contrib import admin
from app.ig.export.xml.models import *

admin.site.register(ExportXml)
admin.site.register(TemplateXml)
