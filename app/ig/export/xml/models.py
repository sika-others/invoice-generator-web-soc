# -*- coding: utf-8 -*- 

import os

from django.db import models
from django.utils.translation import ugettext


# Create your models here.

class ExportXml(models.Model):
    created = models.DateTimeField(auto_now=True)
    content = models.TextField()
    
    class Meta:
        verbose_name = ugettext(u"Export XML")
        verbose_name_plural = ugettext(u"Export XML")
    
class TemplateXml(models.Model):
    name = models.CharField(max_length=64)
    active = models.BooleanField(default=True)
    content = models.TextField()
    item = models.TextField()
    
    class Meta:
        verbose_name = ugettext(u"Šablona XML")
        verbose_name_plural = ugettext(u"Šablony XML")
