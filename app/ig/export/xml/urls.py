from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^download/(?P<xml_pk>\w+)$',
        'app.ig.export.xml.views.download',
        name = "ig.export.xml.download",
    ),
    url(
        r'^in_browser/(?P<xml_pk>\w+)$',
        'app.ig.export.xml.views.in_browser',
        name = "ig.export.xml.in_browser",
    ),
)


