from django.shortcuts import render_to_response, get_object_or_404
from django.template import Template as djangoTemplate
from django.core.context_processors import csrf
from django.template import loader, Context, RequestContext
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.core.urlresolvers import reverse

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache

from app.ig.export.xml.models import *

# Create your views here.

@login_required
@never_cache
def download(request, xml_pk):
    obj = ExportXml.objects.get(pk=xml_pk)
    response = HttpResponse(mimetype='text/xml')
    response['Content-Disposition'] = 'attachment; filename=%s.xml'%obj.created
    response.write(obj.content)
    return response
    
def in_browser(request, xml_pk):
    obj = ExportXml.objects.get(pk=xml_pk)
    response = HttpResponse(mimetype='text/xml')
    response.write(obj.content)
    return response
