from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(
        r'^xml/',
        include('app.ig.export.xml.urls'),
    ),
)


