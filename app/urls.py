from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.generic.simple import redirect_to

import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    ('^$', redirect_to, {'url': '/ig/dashboard/'}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ig/', include("app.ig.urls")),
    (r'^accounts/login/$', 'django.contrib.auth.views.login'),#{'template_name': 'myapp/login.html'}
    url('^accounts/profile/$', redirect_to, {'url': '/dashboard/'}),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
        }),
   )

#urlpatterns += patterns('', url(r'^', include('django.contrib.flatpages.urls')))
